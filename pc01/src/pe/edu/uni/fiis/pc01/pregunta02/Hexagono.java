package pe.edu.uni.fiis.pc01.pregunta02;

public class Hexagono extends Poligono{
    private double apotema;
    private double lado;

    public Hexagono(int numlados, double apotema, double lado) {
        super(6);
        this.apotema = apotema;
        this.lado = lado;
    }

    public double getApotema() {
        return apotema;
    }

    public void setApotema(double apotema) {
        this.apotema = apotema;
    }

    public double getLado() {
        return lado;
    }

    public void setLado(double lado) {
        this.lado = lado;
    }
    
    
    private double peri(){
        return  lado*6;
    }

    private double area(){
        return peri()*apotema/2;
    }
   
    
    private double cantidaddiag(){
        return 6*2/2;
    } 
}