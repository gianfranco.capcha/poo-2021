package pe.edu.uni.fiis.pc01.pregunta02;

public class Triangulo extends Poligono{
    private double lado;
    
    public Triangulo(int numlados,double lado) {
        super(3);
        this.lado = lado;
    }

    public double getLado() {
        return lado;
    }

    public void setLado(double lado) {
        this.lado = lado;
    }

    private double area(){
        return Math.sqrt(3)*Math.pow(lado, 2)/4;
    }

    private double peri(){
        return  lado*3;
    }
    
    private double cantidaddiag(){
        return 0;
    }

}
    

    
    
