package pe.edu.uni.fiis.pc01.pregunta02;

public class Cuadrado extends Poligono{
    private double lado;
    
    public Cuadrado(int numlados,double lado) {
        super(4);
        this.lado = lado;
    }

    public double getLado() {
        return lado;
    }

    public void setLado(double lado) {
        this.lado = lado;
    }

    private double peri(){
        return  lado*4;
    }

    private double area(){
        return Math.pow(lado, 2);
    }
      
    private double cantidaddiag(){
        return 2;
    }

}
    

    
    
