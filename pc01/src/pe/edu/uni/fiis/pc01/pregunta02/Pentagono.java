package pe.edu.uni.fiis.pc01.pregunta02;

public class Pentagono extends Poligono{
    private double apotema;
    private double lado;

    public Pentagono(int numlados, double apotema, double lado) {
        super(5);
        this.apotema = apotema;
        this.lado = lado;
    }

    public double getApotema() {
        return apotema;
    }

    public void setApotema(double apotema) {
        this.apotema = apotema;
    }

    public double getLado() {
        return lado;
    }

    public void setLado(double lado) {
        this.lado = lado;
    }
    
    
    private double peri(){
        return  lado*5;
    }

    private double area(){
        return peri()*apotema/2;
    }
   
    
    private double cantidaddiag(){
        return 5*2/2;
    } 
}
