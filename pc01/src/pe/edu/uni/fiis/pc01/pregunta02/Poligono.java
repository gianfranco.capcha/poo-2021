package pe.edu.uni.fiis.pc01.pregunta02;

public class Poligono {
    private int numlados;

    public Poligono(int numlados) {
        this.numlados = numlados;
    }   
   
    public int getNumlados() {
        return numlados;
    }
    public void setNumlados(int numlados) {
        this.numlados = numlados;
    }
}
