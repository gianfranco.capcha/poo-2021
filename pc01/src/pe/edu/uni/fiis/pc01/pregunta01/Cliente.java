package pe.edu.uni.fiis.pc01.pregunta01;

public class Cliente {
    private String dni;
    private String direccion;
    private String nombre;

    public Cliente(String dni, String direccion, String nombre) {
        this.dni = dni;
        this.direccion = direccion;
        this.nombre = nombre;
    }

    public String getDni() {
        return dni;
    }
    public void setDni(String dni) {
        this.dni = dni;
    }
    public String getDireccion() {
        return direccion;
    }
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
  

}
