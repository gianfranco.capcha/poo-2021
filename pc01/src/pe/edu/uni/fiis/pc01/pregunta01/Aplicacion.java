package pe.edu.uni.fiis.pc01.pregunta01;

public class Aplicacion {
    public static void main(String[] args) {
        Calzado Adidas = new Calzado();
        Adidas.setPrecioUnitario(160); 
        Adidas.setCategoria("Juvenil");
        Adidas.setMarca("Adidas");
        Adidas.setModelo("XYZ");
        Adidas.setTipo("A");
        Adidas.setGenero("Hombre");
        Adidas.setHorma("Plastico");
        Adidas.setMaterial("Polyester");
        Adidas.setTemporada("Verano");
        Adidas.setPaisdeorigen("China");

        Calzado Puma = new Calzado();
        Puma.setPrecioUnitario(550); 
        Puma.setCategoria("Infante");
        Puma.setMarca("Puma");
        Puma.setModelo("ABC");
        Puma.setTipo("B");
        Puma.setGenero("Mujer");
        Puma.setHorma("Plastico");
        Puma.setMaterial("Cuero");
        Puma.setTemporada("Primavera");
        Puma.setPaisdeorigen("Alemania");

        Cliente clientea = new Cliente("25625456","Jr. Los alamos", "Jorge");
        Cliente clienteb = new Cliente("25625456","Jr. Los alamos", "Jorge");

        Venta venta1 = new Venta();
        venta1.setPrecio(160);
        venta1.setProducto(Adidas);
        venta1.setCliente(clientea);

        Venta venta2 = new Venta();
        venta2.setPrecio(550);
        venta2.setProducto(Puma);
        venta2.setCliente(clienteb);

        System.out.println("Venta 1: "+venta1.getPrecio());
        System.out.println("Venta 2: "+venta2.getPrecio());

    }
}
