package pe.edu.uni.fiis.pc01.pregunta01;

public class Venta {
    private double precio;
    private Calzado producto;
    private Cliente cliente;
    public double getPrecio() {
        return precio;
    }
    public void setPrecio(double precio) {
        this.precio = precio;
    }
    public Calzado getProducto() {
        return producto;
    }
    public void setProducto(Calzado producto) {
        this.producto = producto;
    }
    public Cliente getCliente() {
        return cliente;
    }
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    
}
    

