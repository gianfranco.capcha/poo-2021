package pe.edu.uni.fiis.pc01.pregunta03;

public class Evaluacion {
    private String cantidad;
    private int nota;
    private String curso;
    public Evaluacion(String cantidad, int nota, String curso) {
        this.cantidad = cantidad;
        this.nota = nota;
        this.curso = curso;
    }
    public String getCantidad() {
        return cantidad;
    }
    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }
    public int getNota() {
        return nota;
    }
    public void setNota(int nota) {
        this.nota = nota;
    }
    public String getCurso() {
        return curso;
    }
    public void setCurso(String curso) {
        this.curso = curso;
    }
    

}
