CREATE TABLE empleado(
	codigo_empleado VARCHAR(20),
	codigo_departamento VARCHAR(10),
	nombres VARCHAR(50),
	apellidos VARCHAR(100)
);

CREATE TABLE departamento(
	codigo_departamento VARCHAR(10),
	nombre VARCHAR(100),
	ubicacion VARCHAR(50)
);

CREATE TABLE actividad(
	id_actividad NUMERIC(4),
	nombre VARCHAR(50),
	prioridad NUMERIC(1)
);

CREATE TABLE asignacion(
	id_asignacion NUMERIC(5),
	codigo_empleado VARCHAR(20),
	id_actividad NUMERIC(4)),
	presupuesto NUMERIC(9,2)
);

INSERT INTO empleado(codigo_empleado,codigo_departamento,nombres,apellidos) VALUES ('100','1','Juan','Benites');
INSERT INTO empleado(codigo_empleado,codigo_departamento,nombres,apellidos) VALUES ('101','2','David','Perez');

INSERT INTO departamento(codigo_departamento,nombres,ubicacion) VALUES ('1','Lima','Jr. Los alamos 554');
INSERT INTO departamento(codigo_departamento,nombres,ubicacion) VALUES ('2','Junin','Av. Libertad 452');

INSERT INTO actividad(id_actividad,nombre,prioridad) VALUES (10,'Distribucion',6);
INSERT INTO actividad(id_actividad,nombre,prioridad) VALUES (11,'Publicidad',9);

INSERT INTO asignacion(id_asignacion,codigo_empleado,id_actividad,presupuesto) VALUES (1,'100',10,300);
INSERT INTO asignacion(id_asignacion,codigo_empleado,id_actividad,presupuesto) VALUES (2,'101',11,900);

--USANDO CONSULTAS DE MULTIPLES TABLAS
SELECT e.codigo_empleado,e.nombres,e.apellidos,d.nombre
FROM empleado e
JOIN departamento d ON (e.codigo_departamento = d.codigo_departamento);

--1. Obtener los nombres, apellidos y el nombre del departamento
--de todos los empleados.
--2. Registrar una nueva actividad.
--3. Obtener las asignaciones de un determinado empleado.
--4. Actualizar la ubicación de un determinado departamento