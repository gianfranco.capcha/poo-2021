package com.pc3.demo.dao;

import java.util.List;

import com.pc3.demo.dto.Actividad;
import com.pc3.demo.dto.Departamento;
import com.pc3.demo.dto.Empleado;

public interface Dao {
    public List<Empleado> obtenerEmpleado();
    public Actividad agregarActividad(Actividad actividad);
    public Asignacion obtenerAsignacion();
    public Departamento actualizarDepartamento();
}
