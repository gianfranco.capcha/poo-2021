CREATE DATABASE poo;

USE poo;

CREATE TABLE usuario(
 id_usuario NUMERIC(9) PRIMARY KEY,
 nombre VARCHAR(100),
 apellidos NUMERIC(100),
 correo VARCHAR(100),
 administrador char(1),
 clave VARCHAR(200)
 );