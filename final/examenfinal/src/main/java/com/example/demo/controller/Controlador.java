package com.example.demo.controller;

import com.example.demo.dto.Usuario;
import com.example.demo.dto.RespuestaUsuario;
import com.example.demo.servicios.Servicio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = {"*"})
public class Controlador {
    @Autowired
    private Servicio servicio;
    
    @RequestMapping(
        value = "/agregar-usuario",
        method = RequestMethod.POST,
        consumes = "application/json;charset=utf-8",
        produces = "application/json;charset=utf-8"
    )
    public @ResponseBody void agregarusuario(@RequestBody Usuario usuario){
        servicio.agregar_usuario(usuario);
    } 

    @RequestMapping(
        value = "/eliminar-usuario",
        method = RequestMethod.POST,
        consumes = "application/json;charset=utf-8",
        produces = "application/json;charset=utf-8"
    )
    public @ResponseBody void eliminarusuario(@RequestBody Usuario usuario){
        servicio.eliminar_usuario(usuario);
    }

    @RequestMapping(
        value = "/obtener-usuarios",
        method = RequestMethod.POST,
        produces = "application/json;charset=utf-8"
    )
    public @ResponseBody RespuestaUsuario obtenerUsuarios(){
        RespuestaUsuario respuestaUsuario = new RespuestaUsuario();
        respuestaUsuario.setLista(servicio.obtenerUsuarios());
        return respuestaUsuario;
    }
    @RequestMapping(
            value = "/obtener-usuario",
            method = RequestMethod.POST,
            consumes = "application/json;charset=utf-8",
            produces = "application/json;charset=utf-8"
    )
    public @ResponseBody Usuario obtenerUsuario(@RequestBody Usuario usuario){
        return servicio.obtenerUsuario(usuario);
    }

}
