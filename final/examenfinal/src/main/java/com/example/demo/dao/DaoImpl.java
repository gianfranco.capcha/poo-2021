package com.example.demo.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.example.demo.dto.Usuario;

@Repository
public class DaoImpl implements Dao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private Connection conexion;

    private void obtenerConexion(){
        try {
            this.conexion = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    private void cerrarConexion(ResultSet resultado,Statement sentencia){
        try {
            if(resultado != null) resultado.close();
            if(sentencia != null) sentencia.close();
            this.conexion.commit();
            this.conexion.close();
            this.conexion = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void agregar_usuario(Usuario usuario) {
        String sql = " insert into usuario(id_usuario, nombres, apellidos, correo, administrador, clave ) values (nextval(secuencia_usuario),?,?,?,?)";
        try {
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setString(1, usuario.getNombres());
            sentencia.setString(2, usuario.getApellidos());
            sentencia.setString(3, usuario.getCorreo());    
            sentencia.setString(4, usuario.getClave());
            sentencia.executeUpdate();
            cerrarConexion(null,sentencia);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void eliminar_usuario(Usuario usuario) {
        String sql = "delete from usuario where id_usuario = ?";
        try{
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, usuario.getId_usuario());
            sentencia.executeUpdate();
            cerrarConexion(null, sentencia);
        } catch(SQLException throwables) {
            throwables.printStackTrace();
        }
    }

 
    public Usuario extraerUsuario(ResultSet resultado) throws SQLException{
        Usuario usuario = new Usuario(
            resultado.getInt("id_usuario"), 
            resultado.getString("nombres"), 
            resultado.getString("apellidos"), 
            resultado.getString("correo"), 
            resultado.getString("administrador"), 
            resultado.getString("Clave")
            );
            return usuario;
    }

    public List<Usuario> obtenerUsuarios() {
        List<Usuario> lista = new ArrayList<>();
        String sql =" select u.id_usuario, u.nombres, u.apellidos, u.correo, u.administrador, u.clave,\n" +
        " from usuario u\n" +
        " where p.id_usuario = ?";
        
        try {
            obtenerConexion();
            Statement sentencia = conexion.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql);
            while (resultado.next()){
                lista.add(extraerUsuario(resultado));
            }
            cerrarConexion(resultado,sentencia);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return lista;
    }

    public Usuario obtenerUsuario(Usuario usuario) {

        String sql = " select u.id_usuario, u.nombres, u.apellidos, u.correo, u.administrador, u.clave,\n" +
        " from usuario u\n" +
        " where p.id_usuario = ?";
        try {
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, usuario.getId_usuario());
            ResultSet resultado = sentencia.executeQuery();
            while (resultado.next()){
                usuario = extraerUsuario(resultado);
            }
            cerrarConexion(resultado,sentencia);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return usuario;
    }

}
