package com.example.demo.servicios;
import com.example.demo.dto.Usuario;
import java.util.List;

public interface Servicio {
    public List<Usuario> obtenerUsuarios();
    public Usuario obtenerUsuario(Usuario usuario);
    void eliminar_usuario(Usuario usuario);
    void agregar_usuario(Usuario usuario);
    }