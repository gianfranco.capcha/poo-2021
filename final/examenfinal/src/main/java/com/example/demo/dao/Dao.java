package com.example.demo.dao;
import com.example.demo.dto.Usuario;
import java.util.List;

public interface Dao {
    public List<Usuario> obtenerUsuarios();
    public Usuario obtenerUsuario(Usuario usuario);
    void eliminar_usuario(Usuario usuario);
    void agregar_usuario(Usuario usuario);
    }
