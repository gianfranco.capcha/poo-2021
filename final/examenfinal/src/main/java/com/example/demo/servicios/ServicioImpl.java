package com.example.demo.servicios;


import com.example.demo.dao.Dao;
import com.example.demo.dto.Usuario;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ServicioImpl implements Servicio{
    @Autowired
    private Dao daoUsuario;
    public void agregar_usuario(Usuario usuario) {
        daoUsuario.agregar_usuario(usuario);
        
    }

    public void eliminar_usuario(Usuario usuario) {
        daoUsuario.eliminar_usuario(usuario);
        
    }

    public Usuario obtenerUsuario(Usuario usuario) {
        return daoUsuario.obtenerUsuario(usuario);
    }

    public List<Usuario> obtenerUsuarios() {
       
        return daoUsuario.obtenerUsuarios();
    }

}
    

