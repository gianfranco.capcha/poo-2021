package pe.edu.uni.parcial.problema2;

public class Postulantes {
    private String dni;
    private String nombres;
    private String apellidos;
    private Educacion nivel;
    private String dirección;
    private String telefono;
    private Integer edad;
    private Integer nota;
    public Postulantes(String dni, String nombres, String apellidos, Educacion nivel, String dirección, String telefono,
            Integer edad, Integer nota) {
        this.dni = dni;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.nivel = nivel;
        this.dirección = dirección;
        this.telefono = telefono;
        this.edad = edad;
        this.nota = nota;
    }
    public String getDni() {
        return dni;
    }
    public void setDni(String dni) {
        this.dni = dni;
    }
    public String getNombres() {
        return nombres;
    }
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }
    public String getApellidos() {
        return apellidos;
    }
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }
    public Educacion getNivel() {
        return nivel;
    }
    public void setNivel(Educacion nivel) {
        this.nivel = nivel;
    }
    public String getDirección() {
        return dirección;
    }
    public void setDirección(String dirección) {
        this.dirección = dirección;
    }
    public String getTelefono() {
        return telefono;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    public Integer getEdad() {
        return edad;
    }
    public void setEdad(Integer edad) {
        this.edad = edad;
    }
    public Integer getNota() {
        return nota;
    }
    public void setNota(Integer nota) {
        this.nota = nota;
    }
    
}
