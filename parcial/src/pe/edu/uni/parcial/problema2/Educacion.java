package pe.edu.uni.parcial.problema2;


public enum Educacion {
    PRIMARIA("Primaria"),SECUNDARIA("Secundaria"),SUPERIOR("Superior");
    private String nombre;

    private Educacion(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

  

}
