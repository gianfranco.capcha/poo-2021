package pe.edu.uni.parcial.problema1.Busqueda;

import java.util.ArrayList;
import java.util.List;

import pe.edu.uni.parcial.problema1.Libro;


public class Busqueda_titulo implements Busqueda{
    public List<Libro> buscarLibro(List<Libro> lista, String titulo) {
        List<Libro> lista_de_libros = new ArrayList<>();
        for (Libro libro: lista) {
            if(libro.getTitulo().contains(titulo)){
                lista_de_libros.add(libro);
            }
        }
        return lista_de_libros;
    }
}



