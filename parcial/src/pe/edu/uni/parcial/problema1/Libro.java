package pe.edu.uni.parcial.problema1;

public class Libro {
    private String titulo;
    private String autor;
    private Integer codigo;
    public Libro(String titulo, String autor, Integer codigo) {
        this.titulo = titulo;
        this.autor = autor;
        this.codigo = codigo;
    }
    public Libro() {
    }
    public String getTitulo() {
        return titulo;
    }
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    public String getAutor() {
        return autor;
    }
    public void setAutor(String autor) {
        this.autor = autor;
    }
    public Integer getCodigo() {
        return codigo;
    }
    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public void Datoslibro(){
        
        System.out.println();
        System.out.println("Titulo: " + this.titulo);
        System.out.println("Autor: " + this.autor);
        System.out.println("Codigo: " + this.codigo);
        
        System.out.println();
    }
}
