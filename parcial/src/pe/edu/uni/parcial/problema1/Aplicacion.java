package pe.edu.uni.parcial.problema1;
import pe.edu.uni.parcial.problema1.Busqueda.*;
import java.util.ArrayList;
import java.util.List;

import pe.edu.uni.parcial.problema1.Busqueda.Busqueda;

public class Aplicacion {
    public static void main(String[] args) {
        List<Libro> Libro = new ArrayList<>();
        Libro libro = new Libro();
        libro.setAutor("Mario Vargas LLosa");
        libro.setCodigo(100);
        libro.setTitulo("La casa verde");
        
        List<Informacion> info = new ArrayList<>();
        Informacion info_1 = new Informacion();
        info_1.setDescripcion("Segunda novela");
        info_1.setEditorial("TOHO");
        info_1.setAnio_publicación("1966");
        info_1.setTítulos_cap("CAPITULO 1,CAPITULO 2,CAPITULO 3");
        info_1.setResumen("La casa verde relata la historia de un ladrón y contrabandista llamado Fushia que se interna en las selvas peruanas al tratar de huir de la justicia.");
        
        Busqueda busqueda = Busqueda.getBusqueda;
        List<Libro> lista = busqueda.buscarLibro(libro," La casa verde");
        for (Libro libro_1: lista) {
            System.out.println(libro_1.getTitulo());
        }

    }
}
