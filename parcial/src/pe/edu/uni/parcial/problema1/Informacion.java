package pe.edu.uni.parcial.problema1;

public class Informacion {
    private String descripcion;
    private String editorial;
    private String anio_publicación;
    private String títulos_cap;
    private String resumen;
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public String getEditorial() {
        return editorial;
    }
    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }
    public String getAnio_publicación() {
        return anio_publicación;
    }
    public void setAnio_publicación(String anio_publicación) {
        this.anio_publicación = anio_publicación;
    }
    public String getTítulos_cap() {
        return títulos_cap;
    }
    public void setTítulos_cap(String títulos_cap) {
        this.títulos_cap = títulos_cap;
    }
    public String getResumen() {
        return resumen;
    }
    public void setResumen(String resumen) {
        this.resumen = resumen;
    }
    
}
