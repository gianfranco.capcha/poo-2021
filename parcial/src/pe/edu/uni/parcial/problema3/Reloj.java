package pe.edu.uni.parcial.problema3;

public class Reloj {
    Long codigoProducto;
    String nombreProducto;
    int valorProducto;

    public Long getCodigo(){
        return codigoProducto;
    }

    public void setCodigo(Long codigoProducto){
        this.codigoProducto = codigoProducto;
    }
    
    public String getNombre(){
        return nombreProducto;
    }

    public void setNombre(String nombreProducto){
        this.nombreProducto = nombreProducto;
    }
   
    public int getValor(){
        return valorProducto;
    }

    public void setValor(int valorProducto){
        this.valorProducto = valorProducto;
    }
}

