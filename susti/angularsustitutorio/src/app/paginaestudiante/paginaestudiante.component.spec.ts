import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginaestudianteComponent } from './paginaestudiante.component';

describe('PaginaestudianteComponent', () => {
  let component: PaginaestudianteComponent;
  let fixture: ComponentFixture<PaginaestudianteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaginaestudianteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginaestudianteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
