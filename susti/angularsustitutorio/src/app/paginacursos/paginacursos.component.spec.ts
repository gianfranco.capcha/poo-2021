import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginacursosComponent } from './paginacursos.component';

describe('PaginacursosComponent', () => {
  let component: PaginacursosComponent;
  let fixture: ComponentFixture<PaginacursosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaginacursosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginacursosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
