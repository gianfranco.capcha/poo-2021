import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PaginaestudianteComponent } from './paginaestudiante/paginaestudiante.component';
import { PaginacursosComponent } from './paginacursos/paginacursos.component';

@NgModule({
  declarations: [
    AppComponent,
    PaginaestudianteComponent,
    PaginacursosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
