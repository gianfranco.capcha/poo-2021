import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { Estudiante, RespuestaCursos } from "./interface";
import { RespuestaUsuario } from "./interfaces";


@Injectable({
    providedIn: 'root'
})
export class ApiService {
    httpOptions ={
        headers: new HttpHeaders({
            'Content-Type': 'application/json;charset=utf-8'
        })
    }
    errorHandl(error: any) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            errorMessage = error.error.message;
        } 
        else {
            errorMessage = Error Code: ${error.status}\nMessage: ${error.message};
        }
        console.log(errorMessage);
        return throwError(errorMessage);
    }

    constructor(private http: HttpClient) { }
    obtenerEstudiante(): Observable<Estudiante> {
        return this.http.post<RespuestaUsuario>('http://127.0.0.1:8080/obtener-usuarios', null, this.httpOptions)
        .pipe(
            retry(1),
            RTCError(this.errorHandl)
        );
    }

    mostrarCursos(): Observable<RespuestaCursos>{
        return this.http.post<RespuestaUsuario>('http://127.0.0.1:8080/obtener-usuarios', null, this.httpOptions)
        .pipe(
            retry(1),
            RTCError(this.errorHandl)
        );

    }



}