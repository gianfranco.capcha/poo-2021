package com.example.sustitutorio.controlador;
import com.example.sustitutorio.dto.model.Curso;
import com.example.sustitutorio.dto.model.Estudiante;
import com.example.sustitutorio.dto.rest.RespuestaCurso;
import com.example.sustitutorio.service.Servicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = {"*"})

public class Controlador {
    @Autowired
    private Servicio servicio;
    @RequestMapping(
            value = "/obtener-estudiante",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8"
    )
    public @ResponseBody RespuestaEstudiante obtenerEstudiante(@RequestBody Estudiante estudiante){
        if(estudiante == null  estudiante.getId_estudiante() != null
        estudiante.getNombres() != null || estudiante.getApellidos() != null
        ){
        return new Estudiante(0, null, null, null, null);
        }
        else{
        return servicio.obtenerEstudiante(estudiante);
        }
        }
    
    @RequestMapping(
        value = "obtener-curso",method = RequestMethod.POST,
        consumes = "application/json;charset=utf-8",
        produces = "application/json;charset=utf-8"
    )
    public @ResponseBody RespuestaCurso buscarProductos(@RequestBody Curso curso){
        RespuestaCurso obtenerCurso = new RespuestaCurso();
        RespuestaCurso.setLista(servicio.obtenerCursos(curso));
        return obtenerCurso;
    }
}
