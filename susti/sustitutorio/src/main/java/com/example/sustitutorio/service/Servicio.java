package com.example.sustitutorio.service;
import com.example.sustitutorio.dto.model.Estudiante;
import com.example.sustitutorio.dto.model.Curso;
import java.util.List;

public interface Servicio {
    void agregar_estudiante(Estudiante estudiante);
    void eliminar_estudiante(Estudiante estudiante);
    public List<Estudiante> obtenerEstudiantes();
    public Estudiante obtenerEstudiante(Estudiante estudiante);
    public List<Curso> obtenerCursos(Curso curso);
}
