package com.example.sustitutorio.dto.model;

import lombok.Data;

@Data

public class Inscripcion_curso {
    private int id_curso;
    private int id_estudiante;
	private String fecha_incscripcion;
    public Inscripcion_curso(int id_curso, int id_estudiante, String fecha_incscripcion) {
        this.id_curso = id_curso;
        this.id_estudiante = id_estudiante;
        this.fecha_incscripcion = fecha_incscripcion;
    }
    
    public int getId_curso() {
        return id_curso;
    }
    public void setId_curso(int id_curso) {
        this.id_curso = id_curso;
    }
    public int getId_estudiante() {
        return id_estudiante;
    }
    public void setId_estudiante(int id_estudiante) {
        this.id_estudiante = id_estudiante;
    }
    public String getFecha_incscripcion() {
        return fecha_incscripcion;
    }
    public void setFecha_incscripcion(String fecha_incscripcion) {
        this.fecha_incscripcion = fecha_incscripcion;
    }
    
}