package com.example.sustitutorio.dto.model;

import lombok.Data;

@Data

public class Estudiante {
    private int id_estudiante;
    private String nombres;
	private String apellidos;
	private String correo;
    private String clave;
    
    public Estudiante(int id_estudiante, String nombres, String apellidos, String correo, String clave) {
        this.id_estudiante = id_estudiante;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.correo = correo;
        this.clave = clave;
    }

    public Estudiante(String correo, String clave) {
        this.correo = correo;
        this.clave = clave;
    }
    
    public int getId_estudiante() {
        return id_estudiante;
    }
    public void setId_estudiante(int id_estudiante) {
        this.id_estudiante = id_estudiante;
    }
    public String getNombres() {
        return nombres;
    }
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }
    public String getApellidos() {
        return apellidos;
    }
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }
    public String getCorreo() {
        return correo;
    }
    public void setCorreo(String correo) {
        this.correo = correo;
    }
    public String getClave() {
        return clave;
    }
    public void setClave(String clave) {
        this.clave = clave;
    }

    
}