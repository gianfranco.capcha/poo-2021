package com.example.sustitutorio.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.example.sustitutorio.dto.model.Estudiante;
import com.example.sustitutorio.dto.model.Curso;

@Repository
public class DaoImpl implements Dao {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private Connection conexion;
    private void obtenerConexion(){
        try {
            conexion = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    private void cerrarConexion(ResultSet resultado,Statement sentencia){
        try {
            conexion.commit();
            conexion.close();
            conexion = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }


    public void agregar_estudiante(Estudiante estudiante) {
        String sql = " insert into estudiante(id_estudiante, nombres, apellidos, correo clave ) values (nextval(secuencia_usuario),?,?,?)";
        try {
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setString(1, estudiante.getNombres());
            sentencia.setString(2, estudiante.getApellidos());
            sentencia.setString(3, estudiante.getCorreo());    
            sentencia.setString(4, estudiante.getClave());
            sentencia.executeUpdate();
            cerrarConexion(null,sentencia);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void eliminar_estudiante(Estudiante estudiante) {
        String sql = "delete from estudiante where id_estudiante = ?";
        try{
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, estudiante.getId_estudiante());
            sentencia.executeUpdate();
            cerrarConexion(null, sentencia);
        } catch(SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public Estudiante extraerEstudiante(ResultSet resultado) throws SQLException{
        Estudiante estudiante = new Estudiante(
            resultado.getInt("id_estudiante"), 
            resultado.getString("nombres"), 
            resultado.getString("apellidos"), 
            resultado.getString("correo"), 
            resultado.getString("Clave")
            );
            return estudiante;
    }

    public List<Estudiante> obtenerEstudiantes() {
        List<Estudiante> lista = new ArrayList<>();
        String sql =" select e.id_estudiante, e.nombres, e.apellidos, e.correo, e.clave,\n" +
        " from estudiante e\n" +
        " where e.id_estudiante = ?";
        
        try {
            obtenerConexion();
            Statement sentencia = conexion.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql);
            while (resultado.next()){
                lista.add(extraerEstudiante(resultado));
            }
            cerrarConexion(resultado,sentencia);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return lista;
    }

    public Estudiante obtenerEstudiante(Estudiante estudiante) {

        String sql =" select e.id_estudiante, e.nombres, e.apellidos, e.correo, e.clave,\n" +
        " from estudiante e\n" +
        " where e.id_estudiante = ?";
        try {
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, estudiante.getId_estudiante());
            ResultSet resultado = sentencia.executeQuery();
            while (resultado.next()){
                estudiante = extraerEstudiante(resultado);
            }
            cerrarConexion(resultado,sentencia);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return estudiante;
    }

    private Curso extraerCurso(ResultSet resultado) throws SQLException {
        Curso curso = new Curso(
                resultado.getInt("id_curso"),
                resultado.getString("nombre"),
                resultado.getFloat("precio"),
                resultado.getString("fecha_inicio"),
                resultado.getString("fecha_fin")
        );
        return curso;
    }

    public List<Curso> obtenerCursos(Curso curso) {
        List<Curso> lista2 = new ArrayList<>();
        String sql =" select c.id_curso, c.nombre, c.precio, c.fecha_inicio, c.fecha_fin,\n" +
        " from curso c\n" +
        " where c.id_curso = ?";
        
        try {
            obtenerConexion();
            Statement sentencia = conexion.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql);
            while (resultado.next()){
                lista2.add(extraerCurso(resultado));
            }
            cerrarConexion(resultado,sentencia);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return lista2;
    }

}
