package com.example.sustitutorio.dto.model;

import lombok.Data;

@Data

public class Curso {
    private int id_curso;
    private String nombre;
	private float precio;
	private String fecha_inicio;
    private String fecha_fin;
    
    
    public Curso(int id_curso, String nombre, int precio, String fecha_inicio, String fecha_fin) {
        this.id_curso = id_curso;
        this.nombre = nombre;
        this.precio = precio;
        this.fecha_inicio = fecha_inicio;
        this.fecha_fin = fecha_fin;
    }
    
    public Curso(int int1, String string, float float1, String string2, String string3) {
    }

    public int getId_curso() {
        return id_curso;
    }
    public void setId_curso(int id_curso) {
        this.id_curso = id_curso;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public float getPrecio() {
        return precio;
    }
    public void setPrecio(int precio) {
        this.precio = precio;
    }
    public String getFecha_inicio() {
        return fecha_inicio;
    }
    public void setFecha_inicio(String fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }
    public String getFecha_fin() {
        return fecha_fin;
    }
    public void setFecha_fin(String fecha_fin) {
        this.fecha_fin = fecha_fin;
    }

    
}
