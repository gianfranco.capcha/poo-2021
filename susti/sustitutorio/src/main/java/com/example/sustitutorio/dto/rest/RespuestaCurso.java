package com.example.sustitutorio.dto.rest;
import com.example.sustitutorio.dto.model.Curso;
import lombok.Data;

import java.util.List;
@Data
public class RespuestaCurso {
    private List<Curso> lista;
}
