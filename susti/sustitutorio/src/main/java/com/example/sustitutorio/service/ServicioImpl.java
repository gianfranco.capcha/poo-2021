package com.example.sustitutorio.service;
import com.example.sustitutorio.dto.model.Estudiante;
import com.example.sustitutorio.dto.model.Curso;
import com.example.sustitutorio.dao.Dao;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional

public class ServicioImpl implements Servicio{
    @Autowired
    private Dao daoEstudiante;
    private Dao daoCurso;
    public void agregar_estudiante(Estudiante estudiante) {
        daoEstudiante.agregar_estudiante(estudiante);
        
    }

    public void eliminar_estudiante(Estudiante estudiante) {
        daoEstudiante.eliminar_estudiante(estudiante);
        
    }

    public Estudiante obtenerEstudiante(Estudiante estudiante) {
        return daoEstudiante.obtenerEstudiante(estudiante);
    }

    public List<Estudiante> obtenerEstudiantes() {
       
        return daoEstudiante.obtenerEstudiantes();
    }

    public List<Curso> obtenerCursos(Curso curso) {
       
        return daoCurso.obtenerCursos(curso);
    }

}
