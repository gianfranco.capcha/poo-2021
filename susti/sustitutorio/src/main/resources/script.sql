CREATE DATABASE poo;

USE poo;

CREATE TABLE estudiante(
	id_estudiante NUMERIC(9) PRIMARY KEY,
	nombres VARCHAR(100),
	apellidos VARCHAR(100),
	correo VARCHAR(100),
	clave VARCHAR (200)
);

CREATE TABLE inscripcion_curso(
	id_curso NUMERIC(9) PRIMARY KEY,
	id_estudiante numeric(9) PRIMARY KEY,
    fecha_incscripcion VARCHAR(10)
);

CREATE TABLE curso(
	id_curso NUMERIC(9) PRIMARY KEY,
	nombre VARCHAR(200),
	precio NUMERIC(9,2) ,
	fecha_inicio VARCHAR(10),
	fecha_fin VARCHAR (10)
);

INSERT INTO estudiante(id_estudiante,nombres,apellidos,correo,clave) 
VALUES (100,'Luis','Perez','luis@gmail.com','contraluis')
INSERT INTO estudiante(id_estudiante,nombres,apellidos,correo,clave) 
VALUES (101,'Juan','Santos','juan@gmail.com','contrajuan')

